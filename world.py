from BasicVector import Vec2, Vec3, Vec4
import random


class World:

    def __init__(self, windowSize, pointsRange):
        self.step = Vec2(windowSize.w / pointsRange.x, windowSize.h / pointsRange.y)
        self.pointsRange = pointsRange
        self.points = [[random.randint(0, 1) for x in range(pointsRange.x)] for y in range(pointsRange.y)]
        self.neighbours = [[0 for x in range(pointsRange.x)] for y in range(pointsRange.y)]

    def getPoints(self):
        return self.points

    def update(self):
        self.countNeighbours()
        self.applyChanges()

    def countNeighbours(self):
        patterns = [Vec2(-1, -1), Vec2(0, -1), Vec2(1, -1), Vec2(-1, 0), Vec2(1, 0), Vec2(-1, 1), Vec2(0, 1),
                    Vec2(1, 1)]
        for y in range(self.pointsRange.y):
            for x in range(self.pointsRange.x):
                count = 0
                for pattern in patterns:
                    newPos = Vec2(x, y) + pattern
                    if self.isHere(newPos):
                        count += 1
                self.neighbours[y][x] = count

    def applyChanges(self):
        for y in range(self.pointsRange.y):
            for x in range(self.pointsRange.x):
                if self.neighbours[y][x] == 3:
                    if self.points[y][x] != 1:
                        self.points[y][x] = 1
                elif self.neighbours[y][x] > 3 or self.neighbours[y][x] < 2:
                    if self.points[y][x] == 1:
                        self.points[y][x] = 0

    def isHere(self, pos):
        res = False
        if 0 <= pos.x < self.pointsRange.x and 0 <= pos.y < self.pointsRange.y:
            res = self.points[pos.y][pos.x] == 1

        return res

    def __str__(self) -> str:
        return str(self.points)
