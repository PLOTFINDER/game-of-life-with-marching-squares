import pygame
from BasicVector import *


def renderWorld(window, world):
    clearWindow(window)
    points = world.getPoints()
    pointsRange = world.pointsRange
    step = world.step

    renderPoints(window, points, pointsRange, step)
    renderCuts(window, points, pointsRange, step)


def renderPoints(window, points, pointsRange, step):
    for y in range(pointsRange.y):
        for x in range(pointsRange.x):
            pointValue = points[y][x]
            color = pygame.Color(round(255 * pointValue), round(255 * pointValue), round(255 * pointValue))
            pygame.draw.circle(window, color, (x * step.x, y * step.y), 2)


def renderCuts(window, points, pointsRange, step):
    color = pygame.Color(0, 255, 0)
    for y in range(pointsRange.y - 1):
        for x in range(pointsRange.x - 1):
            pointA = points[y][x]
            pointB = points[y][x + 1]
            pointC = points[y + 1][x + 1]
            pointD = points[y + 1][x]

            startPos = []
            endPos = []

            A = pointA > 0.5
            B = pointB > 0.5
            C = pointC > 0.5
            D = pointD > 0.5

            if A and not B and not C and not D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

            elif not A and B and not C and not D:
                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

            elif not A and not B and C and not D:
                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif not A and not B and not C and D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif A and B and not C and not D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x, y * step.y + step.y / 2))

            elif not A and not B and C and D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x, y * step.y + step.y / 2))

            elif A and not B and not C and D:
                startPos.append((x * step.x + step.x / 2, y * step.y))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif not A and B and C and not D:
                startPos.append((x * step.x + step.x / 2, y * step.y))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif A and not B and C and not D:
                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif not A and B and not C and D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif A and B and C and not D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif A and B and not C and D:
                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y + step.y))

            elif A and not B and C and D:
                startPos.append((x * step.x + step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

            elif not A and B and C and D:
                startPos.append((x * step.x, y * step.y + step.y / 2))
                endPos.append((x * step.x + step.x / 2, y * step.y))

            if len(startPos) != 0:
                for i in range(len(startPos)):
                    pygame.draw.line(window, color, startPos[i], endPos[i])


def clearWindow(window):
    window.fill((0, 0, 0))
