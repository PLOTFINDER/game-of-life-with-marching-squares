import pygame
from pygame.locals import *
import time
from BasicVector import Vec2, Vec3, Vec4
from world import World
from renderer import *


def simulate():
    WINDOW_SIZE = Vec4(0, 0, 600, 600)
    POINTS_RANGE = Vec2(60, 60)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    window = pygame.display.set_mode((WINDOW_SIZE.w, WINDOW_SIZE.h))
    pygame.display.set_caption("Marching Squares")
    mainClock = pygame.time.Clock()

    world = World(WINDOW_SIZE, POINTS_RANGE)
    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            world.update()
            renderWorld(window, world)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()